package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	temperature = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Namespace: "golang",
			Name:      "temperature",
			Help:      "This guage reports temperature (degrees C)",
		})
	waterLevel = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Namespace: "golang",
			Name:      "water_level",
			Help:      "This guage reports water level",
		})
	pressure = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Namespace: "golang",
			Name:      "pressure",
			Help:      "This guage reports air pressure (hPa)",
		})
	humidity = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Namespace: "golang",
			Name:      "humidity",
			Help:      "This guage reports humidity (%)",
		})
)

// Define the tempMon array
type tempMonArray struct {
	TempMonArray []tempMon `json:"tempMon"`
}

// Define the struct for the temperature sensor data
type tempMon struct {
	Temperature string `json:"temperature"`
	Pressure    string `json:"pressure"`
	Humidity    string `json:"humidity"`
}

// Define the tankMon array
type tankMonArray struct {
	TankMonArray []tankMon `json:"tankMon"`
}

// Define the struct for the tank water level sensor
type tankMon struct {
	WaterLevel string `json:"waterLevel"`
}

func main() {
	// First, we need to read from the data collected. The data is
	// in two JSON Arrays as follows:
	// - tempMon: This array will have the following elements:
	//   - Temperature
	//   - Pressure
	//   - Humidity
	// - tankMon: This array will have one element:
	//   - waterLevel
	// We read this file in the Go routine to allow for changes in the file
	// to be captured as well.

	// Create the metrics endpoint for prometheus
	rand.Seed(time.Now().Unix())
	http.Handle("/metrics", promhttp.Handler())

	// Register the Prometheus guages
	prometheus.MustRegister(temperature)
	prometheus.MustRegister(waterLevel)
	prometheus.MustRegister(pressure)
	prometheus.MustRegister(humidity)

	go func() {
		for {
			metricsFile, err := ioutil.ReadFile("/data/temp_data")
			if err != nil {
				fmt.Println(err)
			}

			// Create interfaces for the arrays
			tempmon := tempMonArray{}
			tankmon := tankMonArray{}

			// Unmarshal the tempMon JSON structure and catch errors
			err2 := json.Unmarshal(metricsFile, &tempmon)
			if err2 != nil {
				fmt.Println("Error with the JSON data")
				fmt.Println(err2.Error())
			}

			// // Debug print statements
			// fmt.Println("Temperature: " + tempmon.TempMonArray[0].Temperature)
			// fmt.Println("Pressure: " + tempmon.TempMonArray[0].Pressure)
			// fmt.Println("Humidity: " + tempmon.TempMonArray[0].Humidity)

			// Unmarshal the tankMon JSON structure and catch errors
			err3 := json.Unmarshal(metricsFile, &tankmon)
			if err3 != nil {
				fmt.Println("Error with the JSON data")
				fmt.Println(err3.Error())
			}

			// Set the variables. They need to be float64. So we do some string to
			// float64 conversions while setting the variables.
			temp, _ := strconv.ParseFloat(tempmon.TempMonArray[0].Temperature, 64)
			pres, _ := strconv.ParseFloat(tempmon.TempMonArray[0].Pressure, 64)
			hum, _ := strconv.ParseFloat(tempmon.TempMonArray[0].Humidity, 64)
			water, _ := strconv.ParseFloat(tankmon.TankMonArray[0].WaterLevel, 64)

			// Set the prometheus values using the variables
			temperature.Set(temp)
			waterLevel.Set(water)
			pressure.Set(pres)
			humidity.Set(hum)
			time.Sleep(time.Second)
		}
	}()
	log.Fatal(http.ListenAndServe(":8080", nil))
}
