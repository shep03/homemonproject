#!/usr/bin/env python

import bme680
import json
import time

print("""temperature-pressure-humidity.py - Displays temperature, pressure, and humidity.

Press Ctrl+C to exit

""")

try:
    sensor = bme680.BME680(bme680.I2C_ADDR_PRIMARY)
except IOError:
    sensor = bme680.BME680(bme680.I2C_ADDR_SECONDARY)


sensor.set_humidity_oversample(bme680.OS_2X)
sensor.set_pressure_oversample(bme680.OS_4X)
sensor.set_temperature_oversample(bme680.OS_8X)
sensor.set_filter(bme680.FILTER_SIZE_3)

print('Polling, and writing data to /data/temp_data:')


def json_output():
    try:
      while True:
        time.sleep(2)
        ### Each loop, we want to initialize the array. There
        ### should only ever be one loop worth of results.
        data = {}
        data['tempMon'] = []

        ### We need a placeholder for tankMon. We'll just
        ### set some placeholder values for now.
        data['tankMon'] = []
        data['tankMon'].append({'waterLevel': str(100)})

        if sensor.get_sensor_data():
           data['tempMon'].append({
              'temperature': str(sensor.data.temperature),
              'pressure': str(sensor.data.pressure),
              'humidity': str(sensor.data.humidity)})
           with open('/data/temp_data', 'w') as outfile:
             json.dump(data, outfile)
             outfile.close()
             pass
    except KeyboardInterrupt:
      pass

json_output()


### Can be used to debug stuff.

#def min_max():
#    min = sensor.data.temperature
#    max = 0
#    try:
#      while True:
#        if sensor.get_sensor_data():
#           if sensor.data.temperature > max:
#              max = sensor.data.temperature
#           elif sensor.data.temperature < min:
#              min = sensor.data.temperature
#           print("Min: {0:.2f}C, Max: {1:.2f}C, Current: {4:.2f}C, Pressure (hPa): {2:.3f}, Humidity: {3:.3f}%".format(
#                min,
#                max,
#                sensor.data.pressure,
#                sensor.data.humidity,
#                sensor.data.temperature))
#    except KeyboardInterrupt:
#      pass

#min_max()
