#!/usr/bin/env bash
## Hacky script to manually control the fan speeds on a Dell R610
## We start by collecting the temp from lm_sensors. Then, depending
## on the temperature, we set the fan speed accordingly.
## If it starts to get above 65 degrees C, then we'll send a telegram
## notification. Over 70 and we disable manual control and let the idrac
## take over.

get_temps(){
## We could do average temp. But let's just get the highest temp and work with that:
temp=$(sensors | awk '{print $3}' | egrep -v adapter | cut -f 2 -d + | cut -f 1 -d . | sort -r | head -n 1)
}



check_temp_set_fan(){
 if [[ $temp -ge '65' ]];
      then logger "CPU Temperature is too high: $temp degrees! Setting higher fan speed";
           logger "Running: ipmitool raw 0x30 0x30 0x02 0xff 0x35"
           ipmitool raw 0x30 0x30 0x02 0xff 0x35
           send_alert
 elif [[ $temp -ge '51' && $temp -lt '64' ]];
      then logger "CPU Temp is getting high: $temp degrees. Increasing fan speed";
           logger "Running: ipmitool raw 0x30 0x30 0x02 0xff 0x25"
           ipmitool raw 0x30 0x30 0x02 0xff 0x25
 elif [[ $temp -ge '70' ]];
      then logger "CPU Temp is too high: $temp degrees. Disabling manual fan control";
           logger "Running: ipmitool raw 0x30 0x30 0x01 0x01"
           ipmitool raw 0x30 0x30 0x01 0x01
 elif [[ $temp -ge '45' && $temp -lt '50' ]];
      then logger "CPU Temp is getting high: $temp degrees. Increasing fan speed";
           logger "Running: ipmitool raw 0x30 0x30 0x02 0xff 0x20 and setting manual fan control"
           ipmitool raw 0x30 0x30 0x01 0x00
           ipmitool raw 0x30 0x30 0x02 0xff 0x20
 else logger "CPU temp is looking good: $temp degrees. Setting stealth mode";
         logger "Running: ipmitool raw 0x30 0x30 0x02 0xff 0x1e"
         ipmitool raw 0x30 0x30 0x02 0xff 0x1e
 fi
}

send_alert(){
  #### If the temp is too high, let's send a Telegram notification
  message="Temp of $(hostname) is currently at ${temp}c"
  if [[ $temp ]];
  then
        curl -k -f -H 'Content-Type: application/json' \
        -XPOST --data "{\"extra_vars\": {\"tg_msg\": \"$message\"}}" \
        --user admin:PASSWORD \
        http://awx.k8s.bne-home.net/api/v2/job_templates/16/launch/
  fi

}
get_temps
check_temp_set_fan
